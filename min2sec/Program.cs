﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace min2sec
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Type a timestamp");
            string time = Console.ReadLine();
                while (time != "exit") {
                    while (time.Length != 4 || !int.TryParse(time, out int n))
                    {
                        Console.WriteLine("Pls type a proper number ._ .");
                        Console.Write("A number pls[annoyed]: ");
                        time = Console.ReadLine();
                    }
               
                int result = GetTime(time);
                    string ans = result > 1003 ? "That timestamp doesn't exist. Please, try another time." : "https://youtu.be/vc1E5CfRfos?t=" + result.ToString();
                    Console.WriteLine(ans);
                    Clipboard.SetText(ans);
                    Console.Write("Time: ");
                    time = Console.ReadLine();
                }
        }

        static public int GetTime(string time)
        {
            if (!time.Contains(':'))
            {
                char[] timeChar = time.ToCharArray();
                string min = timeChar[0].ToString() + timeChar[1].ToString();
                string sec = timeChar[2].ToString() + timeChar[3].ToString();
                int result = (Int32.Parse(min) * 60) + Int32.Parse(sec);
                return result;
            }
            //make char global variable
            char[] car = new char[] { ':' };
            string[] timeArr = new string[] { };
            timeArr = time.Split(car);
            int minutes = Int32.Parse(timeArr[0]);
            int seconds = Int32.Parse(timeArr[1]);
            return (minutes * 60) + seconds;
        }
    }
}
